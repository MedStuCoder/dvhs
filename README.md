# DVHS

Deprecated due to complications (e.g the fact that I'm learning Python while coding and I now know that this code needs to be rewritten and Git repos should look much better) and change of plans, might reboot in future.

Python program for converting any given file to video recordable to VHS cassette with error correction and color calibration to use the VHS tape similar to an Linear Tape-Open magnetic tape to save digital data.

I'm still not sure about the lisence so if you have any suggestions PLEASE let me know.




Incoming changes :

- [ ] Fix the necessity to put the entire file in the RAM `(Upcoming commit)`


- [ ] Fix the IndexError catching debauchery `(Upcoming commit)`
 
 
- [ ] Integrate FFmpeg (Upcomming commit)
 

- [ ] Add Reed-Solomon (?)
 

- [ ] Fix Last frame verification data ending signature (Which will help later in the video Reading script)
 

- [ ] Remove rjust since FFmpeg is cool and can take the frame names normally
 

- [ ] Fix naming issue due to CalibCount
 

- [ ] Fix frameSkipper interfering with verification
 

- [ ] Add ability to call from console and/or drag and drop file
 

- [ ] ~~Add audio channel error correction (potentially writing a new audio MODEM cuz everything else is either lisenced weirdly or I can't read and modify the code)~~ Too many problems with reading the audio from the medium and decoding it
 

- [x] FFMPEG or use pure Python for joining the frames together and adding the audio `(FFmpeg chosen)`
 

- [ ] Adding Resources folder to clean things up `(Upcomming commit)`
 

- [ ] Adding the video reader script


