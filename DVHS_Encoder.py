from PIL import Image
from time import time #,sleep
from Color_Console import color
from shutil import copyfile
from os import system
#from json import dumps #For WriteToFile part, see below


##hackerMan() #Console color for Windows
color ( text = "bright white" , bg = "black" , delay = 0.67 ,repeat = -1 , dict = {} )


#INTRAFRAME : bits of change to the code for an upgrade considered for next version : For intraframe color calibration instead of interframe
#MEMCLS : bits of change to the code for an upgrade considered for next version : To clear the Bytelists filled to the RAM as they are written to pixels


ipix=768 #Frame width
jpix=576 #Frame height
fps=24 #Frames per second, used for interframe color calibration



address = "Yourfile.yourfileformat"

#droppedFile = sys.argv[1] #Importing input file by drag and drop, Idk how to do this yet.

with open(address, "rb") as img_file:  #rb-r
     
    data = img_file.read()

img_file.close()


bytelist = list (data)

bytelistR = bytelist [::3] 
bytelistG = bytelist [1::3]
bytelistB = bytelist [2::3]



#writeToFileFlag = input("Write To File?")

#Write To File considered for a possible future version
#where a statically typed compiled language like
#GoLang takes over the writing bytes to pixels part
#Or maybe even a full rewrite of the code in that language
#Mostly for fun but would speed up the process dramatically
#Nevertheless you can uncomment this part along with the flag to get a text output:

##def WTF(bytelistR,bytelistG,bytelistB):
##     beginWTF = time()
##     
##     bytelistR = dumps(bytelistR)
##     bytelistG = dumps(bytelistG)
##     bytelistB = dumps(bytelistB)
##
##     with open('bytelistR.txt', 'w') as fileR:
##          #fileR.writelines("%s\n" % place for place in bytelistR)
##          fileR.write(bytelistR)
##          fileR.close()
##     with open('bytelistG.txt', 'w') as fileG:
##          #fileG.writelines("%s\n" % place for place in bytelistG)
##          fileG.write(bytelistG)
##          fileG.close()
##     with open('bytelistB.txt', 'w') as fileB:
##          #fileB.writelines("%s\n" % place for place in bytelistB)
##          fileB.write(bytelistB)
##          fileB.close()
##     endWTF = time()
##     print (endWTF - beginWTF)
##     input("Any key to exit")
##     exit()

##if writeToFileFlag == "1":
##     WTF(bytelistR,bytelistG,bytelistB)



#Verification happens by reading the data back from the created frames after every frame is created
verFlag = input("""Verify? 1 for Yes, any else for No (Will take roughly 2.75 times more time):""")

if verFlag == (""):
     verFlag = 0
if verFlag == ("1"):
     print("You'll only be notified of an error in verification if there is one")

frameSkipper = input("Skip To Frame Number?") #Currently BROKEN, Interferes with the color calibration frames
if frameSkipper == (""):
     frameSkipper = "0"
     
calibCount = int(int(frameSkipper)/fps) #Calculates the number of necessary calibration frames
print(calibCount)


print ("Total BYTES :", len (bytelist))
print ("Total PIXELS:", len (bytelistR))

frames = int(len(bytelistR)/(ipix*jpix))
print ("Total FRAMES:", frames)

bytelist.clear() #Clear some memory
del bytelist

input("CONTINUE?")

framePadding = len(str(frames))  #Necessary for FFMPEG to detect the frames



#Progress bar from : https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console :
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
     if total == 0:
          total = 1
     percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
     filledLength = int(length * iteration // total)
     bar = fill * filledLength + '-' * (length - filledLength)
     print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
     # Print New Line on Complete
     if iteration == total:
          print()

     
     """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
     """



def main() :

     i = 0
     j = 0
     y=ipix*jpix
     calibCount = 0 #int(frameSkipper/fps)+1


     for f in range(int(frameSkipper), frames+1):

          if (f%fps == 0):
               calibCount =calibCount+1
               copyfile("Calibration.png", str(f).rjust(framePadding, "0") + " " + address + ".png")

          printProgressBar (f, frames) 
                   
          img = Image.new( 'RGB', (ipix,jpix), "red") #Create a new frame
          
          pixels = img.load() #Create the pixel map



#INTRAFRAME - Planned for future - DOES NOT WORK:
#We define regions of the frames
#whose color we know and can then calibrate the rest based on
          
#...feels a little too much
          
##          for leftborderH in range (jpix):
##               for leftborderW in range(2):
##                    pixels[leftborderW ,leftborderH] = (0, 255, 0)
##               
##          for RborderH in range (jpix):
##               for RborderW in range(ipix-2, ipix):
##                    pixels[RborderW ,RborderH] = (0, 0, 255)



          z = f*y  #disable if memory cleaner is used, not working, refer below comment #MEMCLS
          
          for x in range (y) :
               
               l = x+z #disable if memory cleaner is used, not working, refer below comment #MEMCLS
               try:
                    
                    charR = bytelistR[l]     #use x for memory cleaner: Doesn't work : IndexError : Too lz 2 Dbug #MEMCLS
                    charG = bytelistG[l]     #use x for memory cleaner: Doesn't work : IndexError : Too lz 2 Dbug #MEMCLS
                    charB = bytelistB[l]     #use x for memory cleaner: Doesn't work : IndexError : Too lz 2 Dbug #MEMCLS
                    
                    
               except IndexError: #This detects the very last subpixel
                    blBFlag = 0
                    blGFlag = 0
                    l = l+1
                    
                    if l > len(bytelistG):
                         #print ("G TRIGERRED")
                         blGFlag = 1
                         charG = 0
                         charB = 0
                         
                    elif l > len(bytelistB):
                         blBFlag = 1
                         #print ("B TRIGERRED")
                         charB=0
                    
                    pixels[i,j] = (charR, charG, charB)
                    
                    
                    while i<ipix-5:  #INTRAFRAME - Not yet
                         i=i+1
                         pixels[i,j] = (255, 255, 255)
                         i=i+1
                         pixels[i,j] = (255, 0, 0)
                         i=i+1
                         pixels[i,j] = (0, 255, 0)
                         i=i+1
                         pixels[i,j] = (0, 0, 255)
                    
                    if j < jpix:
                         j = j+1
                         i=0
                         while i<ipix:   #INTRAFRAME - Not yet
                              pixels[i,j] = (0, 0, 255)
                              i = i+1
                              
                    img.save(str(f+calibCount).rjust(framePadding, "0") + " " + address + ".png")
                    
                    if verFlag == "1":
                         
                         imgV = Image.open(str(f+calibCount).rjust(framePadding, "0") + " " + address + ".png")
                         rgb_imgV = imgV.convert('RGB')

                         bytelistBV = []
                         bytelistRV = []
                         bytelistGV = []
                         for jv in range (jpix): #(2, jpix): #INTRAFRAME

                              for iv in range (ipix): #(2, ipix-2): #INTRAFRAME
                                   
                                   charRV, charGV, charBV = rgb_imgV.getpixel((iv, jv))
                                   
                                   if charRV == charGV == charBV == 255: #This piece of code looks for a white pixel, and then makes sure the next 3 pixels are
                                                                                                     #Red, Green, and Blue respectively which means we have reached the end of the last frame
                                                                                                     #However, if there aren't enough number of pixels left in the current row to make sure of that,
                                                                                                    #the program will tell you that it could not verify the last frame, but it should be OK.
                                        if ipix-iv >3:
                                             iv2 = iv+1
                                             charRV2, charGV2, charBV2 = rgb_imgV.getpixel((iv2, jv))
                                             if charRV2 == 255 and charGV2 == charBV2 == 0:
                                                  iv2 = iv2+1
                                                  charRV2, charGV2, charBV2 = rgb_imgV.getpixel((iv2, jv))
                                                  if charGV2 == 255 and charRV2 == charBV2 == 0:
                                                       iv2 = iv2+1
                                                       charRV2, charGV2, charBV2 = rgb_imgV.getpixel((iv2, jv))
                                                       if charBV2 == 255 and charRV2 == charGV2 == 0:
                                                            print("LAST FRAME VERIFICATION")
                                                            #print(blBFlag,blGFlag)
                                                            
                                                            if blGFlag == 1:
                                                                 #print ("G TRIGERRED - VERIF")
                                                                 bytelistG.append(0)
                                                                 bytelistB.append(0)
                                                                 0==0
                                                            elif blBFlag == 1:
                                                                 0==0
                                                                 #print ("B TRIGERRED - VERIF")
                                                                 bytelistB.append(0)

                                                            
                                                            if bytelistRV != bytelistR[(y*f):((y*f)+(y))] or bytelistGV != bytelistG[(y*f):((y*f)+(y))] or bytelistBV != bytelistB[(y*f):((y*f)+(y))]:
                                                                 print("VERIFICATION FAULT AT FRAME", f, "!!!")

                                                            
                                                            passed = time()
                                                            p2 = passed - begin
                                                            print ("Total time spent", p2)
                                                            input("Any key to exit")
                                                            exit()
                                        else: #NONONONONO THIS PART IS BAD IT IS GOING TO CAUSE PROBLEMS, FATAL ERROR LOL
                                             print("The last frame could not be verified")
                                             print("However this is not due to an error")
                                             print("It happened because I had to sacrifice the verfication,")
                                             print("To make sure part of your data isn't lost DUE TO it.")
                                             print("Future upgrades should fix this.")
                                             print('Check the part of the GitLab page ReadMe with the "Last frame verification" title')
                                             print("Sorry for the incovenience.")
                                        
                                   bytelistRV.append(charRV)
                                   bytelistGV.append(charGV)
                                   bytelistBV.append(charBV)
##                         print(bytelistRV == bytelistR[(y*f):((y*f)+(y))],bytelistGV == bytelistG[(y*f):((y*f)+(y))],bytelistBV == bytelistB[(y*f):((y*f)+(y))])
                                             
                         
                         imgV.close
                         del imgV
                         
                    passed = time()
                    p2 = passed - begin
                    print ("Total time spent", p2)
                    input("Any key to exit")
                    exit()
                    
               
               pixels[i,j] = (charR, charG, charB)
               
               #img.putpixel((i, j), (charR, charG, charB)) #Alternative code, don't know the diff, seems the same
               
               #The all glorious but broken memory cleaner : #MEMCLS
               
               #del bytelistR[0:(y)]
               #del bytelistG[0:(y)]
               #del bytelistB[0:(y)]
               
               i = i+1 #one pixel column ahead
               
               if (i == ipix): #at the end of pixel column  #ipix-2 : INTRAFRAME
                    i = 0 #go back to the beginning #INTRAFRAME
                    j = j+1 #of the next row
               if (j == jpix): #INTRAFRAME
                  #f = f+1
                    img.save(str(f+calibCount).rjust(framePadding, "0") + " " + address + ".png")
                    i=0
                    j=0
                    #sleep(5)
               
                    ##print("X:",x)

                    
          #verify(y,f)
          if verFlag == "1":
               
               imgV = Image.open(str(f+calibCount).rjust(framePadding, "0") + " " + address + ".png")
               rgb_imgV = imgV.convert('RGB')

               bytelistBV = []
               bytelistRV = []
               bytelistGV = []
               for jv in range (jpix): #INTRAFRAME
                    
                    for iv in range (ipix):
                         
                         charRV, charGV, charBV = rgb_imgV.getpixel((iv, jv))
                         bytelistRV.append(charRV)
                         bytelistGV.append(charGV)
                         bytelistBV.append(charBV)
               if bytelistRV != bytelistR[(y*f):((y*f)+(y))] or bytelistGV != bytelistG[(y*f):((y*f)+(y))] or bytelistBV != bytelistB[(y*f):((y*f)+(y))]:
                    print("VERIFICATION FAULT AT FRAME", f, "!!!")
               
               imgV.close
               del imgV
               #input("PAUSE")
          
begin = time()

main()

system("ffmpeg -r 24 -i %02d.png -c:v libx264 -crf 0 -f mp4 DVHSOutput.mp4")
#crf 0 will probably be unreadable by most video players so we need to 
#increase the crf to like 10(?) and implement CRC32 to compensate for the colors changing 
#And save that data to elsewhere (not the tape) and then use it to correct the colors(maybe?)
#Or maybe we should decrease the resolution to decrease bitrate so the file can be read
#But if that doesn't work either we'll have to come up with a way to keep the resolution BUT use
#more pixels for the same 3 bytes (like in a 2*2 square) so the data can survive the compression
#Tbh this is a major trial and error project, I hope I actually get it to work eventually

